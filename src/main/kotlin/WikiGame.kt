import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import java.util.concurrent.ConcurrentHashMap


/**
 * Смотреть доклад тут: https://jokerconf.com/talks/726fdf3540524be28c8f5901c1800583
 * Репозиторий тут: https://github.com/point-rar/Thread-Wars-WikiGame/blob/main/src/main/kotlin/rar/kotlin/repository/WikiGameCoroImpl.kt
 */

interface WikiGame {
    fun play(startPageTitle: String, endPageTitle: String, maxDepth: Int): List<String?>
}

data class Page(val title: String, val parentPage: Page?) : Comparable<Page> {
    override fun compareTo(other: Page): Int {
        return title.compareTo(other.title)
    }
}


interface WikiRemoteDataSource {
    suspend fun getLinksByTitle(title: String): List<String>

    suspend fun getBacklinksByTitle(title: String): List<String>
}

class WikiGameCoroImpl : WikiGame {
    private val wikiRemoteDataSource: WikiRemoteDataSource = TODO()

    override fun play(startPageTitle: String, endPageTitle: String, maxDepth: Int): List<String> = runBlocking {
        val visitedPages: MutableMap<String, Boolean> = ConcurrentHashMap()

        val startPage = Page(startPageTitle, null)

        val resultPage = processPage(startPage, endPageTitle, 0, maxDepth, visitedPages)

        val path = mutableListOf<String>()

        var curPg: Page? = resultPage
        do {
            path.add(curPg!!.title)
            curPg = curPg.parentPage
        } while (curPg != null)

        return@runBlocking path.reversed()
    }

    private suspend fun processPage(
        page: Page,
        endPageTitle: String,
        curDepth: Int,
        maxDepth: Int,
        visitedPages: MutableMap<String, Boolean>,
    ): Page {
        if (visitedPages.putIfAbsent(page.title, true) != null) {
            throw RuntimeException("Already visited")
        }

        if (page.title == endPageTitle) {
            return page
        }

        if (curDepth == maxDepth) {
            throw RuntimeException("Depth reached")
        }

        val links = wikiRemoteDataSource.getLinksByTitle(page.title)

        val pageChannel = Channel<Page>()

        val scope = CoroutineScope(SupervisorJob() + CoroutineExceptionHandler { _, _ -> })
        links.forEach { link ->
            scope.launch {
                val pageResult = processPage(
                    Page(link, page),
                    endPageTitle,
                    curDepth + 1,
                    maxDepth,
                    visitedPages,
                )

                pageChannel.send(pageResult)
            }
        }

        val resultPage = pageChannel.receive()

        scope.cancel()

        return resultPage
    }
}