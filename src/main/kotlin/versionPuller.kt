import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import java.net.URI

/**
 * Исходник: https://gist.github.com/Mapkn3/4e8407bf6de64271f4367972024fe64d
 */

private const val token = "********"
private const val BASE_API_URL = "http://gitlab.com/api/v4"
private const val SECURITY_QUERY_PARAM = "private_token=${token}"
private const val GET_PROJECT_BY_NAME_PATH_TEMPLATE = "$BASE_API_URL/projects?search=%s&$SECURITY_QUERY_PARAM"
private const val GET_ALL_TAGS_BY_PROJECT_ID_PATH_TEMPLATE = "$BASE_API_URL/projects/%d/repository/tags?$SECURITY_QUERY_PARAM"

fun main() {
    val json = Json { ignoreUnknownKeys = true }
    listOf(
        "ProjectA",
        "ProjectB",
        "ProjectC",
    ).forEach { projectName ->
        val projectsJson = httpGet(GET_PROJECT_BY_NAME_PATH_TEMPLATE.format(projectName))
        val projects = json.decodeFromString<List<Project>>(projectsJson)

        projects
            .filter { it.name == projectName }
            .forEach {
                val tagsJson = httpGet(GET_ALL_TAGS_BY_PROJECT_ID_PATH_TEMPLATE.format(it.id))
                val tags = json.decodeFromString<List<Tag>>(tagsJson)
                val lastTag = tags.map(Tag::name).maxWith(::semVerComparator)

                println("Project ${it.name}: last tag = $lastTag")
            }
    }
}

fun httpGet(url: String): String = URI.create(url).toURL().readText()

fun semVerComparator(a: String, b: String): Int {
    val aParts = a.split('.').map(String::toInt)
    val bParts = b.split('.').map(String::toInt)
    for (i in 0..2) {
        if (aParts[i] > bParts[i]) {
            return 1
        }
        if (aParts[i] < bParts[i]) {
            return -1
        }
    }
    return 0
}

@Serializable
data class Project(val id: Int, val name: String)

@Serializable
data class Tag(val name: String, val message: String)