class JvmLruCache<K, V>(val cacheSize: Int) : LinkedHashMap<K, V>() {
    override fun removeEldestEntry(eldest: MutableMap.MutableEntry<K, V>?): Boolean = size >= cacheSize
}


class KotlinLruCache<K, V>(
    val cacheSize: Int,
    private val content: HashMap<K, V> = hashMapOf(),
) : MutableMap<K, V> by content {

    private var deque = ArrayDeque<K>()

    override fun put(key: K, value: V): V? {
        val newValue = content.put(key, value)
        //if it is a new value
        if (newValue == null) {
            deque.addLast(key)
            while (size > cacheSize) {
                val k = deque.removeFirst()
                content.remove(k)
            }
        }
        //do not use let here
        return newValue
    }
}
