package com.jokerconf.kotlin.idiomatik.conference

import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.minutes

object ConferenceUtils {
    val defaultTalkLength = 45.minutes

    fun endTime(activity: Activity, timeSlot: TimeSlot): Instant = timeSlot.startTime + activity.duration

}