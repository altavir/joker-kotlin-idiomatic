package com.jokerconf.kotlin.idiomatik.conference

import kotlinx.datetime.Instant
import kotlin.time.Duration

data class Organization(val name: String)

data class Abstract(val content: String)


interface Activity {
    val id: String
    val duration: Duration
}

data class Location(
    val id: String,
)

data class TimeSlot(
    val startTime: Instant,
    val location: Location,
)

data class Talk(
    override val id: String,
    val speaker: Speaker,
    override val duration: Duration = ConferenceUtils.defaultTalkLength,
    val abstract: Abstract? = null,
) : Activity

class ScheduleValidationException(
    val first: Pair<TimeSlot, Activity>,
    val second: Pair<TimeSlot, Activity>,
) : Exception()

class Schedule(
    val program: MutableList<Pair<TimeSlot, Activity>> = ArrayList(),
) {
    fun validate() {
        //TODO тут ошибка, даже две
        for (pair1 in program) {
            for (pair2 in program) {
                if (pair1.first.location == pair2.first.location &&
                    (pair1.first.startTime in pair2.first.startTime..(pair2.first.startTime + pair2.second.duration) ||
                            pair2.first.startTime in pair1.first.startTime..(pair1.first.startTime + pair1.second.duration))
                ) {
                    throw ScheduleValidationException(pair1, pair2)
                }
            }
        }
    }
}