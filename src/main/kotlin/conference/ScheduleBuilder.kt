package com.jokerconf.kotlin.idiomatik.conference

import kotlinx.datetime.Instant
import kotlin.time.Duration

class ScheduleBuilder {
    private val program: MutableList<Pair<TimeSlot, Activity>> = ArrayList()


    fun addTalk(
        id: String,
        speaker: Speaker,
        location: Location,
        startTime: Instant,
        duration: Duration = ConferenceUtils.defaultTalkLength,
        abstract: Abstract,
    ): ScheduleBuilder {
        program.add(
            TimeSlot(startTime, location) to Talk(id, speaker, duration, abstract)
        )
        return this
    }

    fun build(): Schedule = Schedule(program)
}