package com.jokerconf.kotlin.idiomatik.conference

class Speaker {
    var name: String? = null
    var surname: String? = null

    var info: String? = null

    var organization: Organization? = null
}