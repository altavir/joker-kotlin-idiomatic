import kotlin.test.Test
import kotlin.test.assertEquals

class KotlinLruCacheTest {
    @Test
    fun cacheOverflow() {
        val cache = KotlinLruCache<Int, Int>(10)
        repeat(12) {
            cache[it] = it
        }

        assertEquals(10, cache.size)
    }
}