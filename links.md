## Идиомы Kotlin
https://kotlinlang.org/docs/idioms.html

## Русскоязычное Kotlin-сообщество
https://t.me/kotlin_lang

## Российское Kotlin-сообщество

(Локальные события и кооперация)

https://t.me/kotlin_russia

## Международное Kotlin сообщество 

(Форма для регистрации)

https://kotl.in/slack

## Kotlin-форум

https://discuss.kotlinlang.org/

## Курс по идиоматичному Kotlin в МФТИ

https://t.me/kotlin_mipt

## Йошкин кот (Kodee)!

![](images/Kodee.png)